mode='query' # Uses the queries list
#mode='url' # Uses query_urls (Not Recomended)
queries = [
        {'q':"python AND (sqlite OR mysql) title:learn NOT title:sleep","sort":"new","limit":100},
        {'q':"funny AND (lol OR lmao) NOT title:sleep","sort":"top","limit":100}
        ]

query_urls = [
        "https://www.reddit.com/search.json?q=pizza%20OR%20bbq%20title%3A(join%20us)"
        ]


rounds = 3 # How many search page  ( Must be 1 if you use the "after" parameter in your query)

# All words in scoring dicts must be lowercase, can be exact phrase of multiple words
# Score has no maximum, it's up to you to specify your scoring range

body_scoring   =   {
               'wow':9, # a very important word
               'i think':5, # a quite important
               'hope':2, # a less important word
               'wtf':-4, # a bad word
               'earth is flat':-6 # an awful word
              }

title_scoring={
               'justice':10,
               'join us':7,
               'chat':2,
               'depression':-5
              }

scoring_threshold = 1.5 # combined = (body_score * scoring_threshold) + title_score

ua = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0'} # PS: Reddit blocks requests from some UAs
db = 'snoo.db' # Sqlite3 DB file path
