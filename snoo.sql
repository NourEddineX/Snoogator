CREATE TABLE posts (id INT PRIMARY KEY, fullname VARCHAR(32) UNIQUE, subreddit VARCHAR(64),url VARCHAR(256), title VARCHAR(512), body VARCHAR(32768), score INT, body_score INT, title_score INT, epoch INT, author VARCHAR(32), after VARCHAR(32));

