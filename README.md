# Snoogator

### Better way to search Reddit

---
### Usage

- Clone the repo
- Run `python3 createdb.py`.
- Add one or more queries / query urls in `snooconf.py`.
- Specify a positive/negative score for each keyword in `body_scoring` and/or `title_scoring` in `snooconf.py` file.
- Run `python3 snoogator.py`.
- Sort Reddit search results by keyword occurence in the post body and/or title from the SQLite3 database.

### SQLite3 query examples
Get Post's title,url,score,body score,title score, and date for the first 15 posts sorted by score.
```
SELECT title,url,score,body_score,title_score,DATE(epoch,'unixepoch') FROM posts ORDER BY score DESC LIMIT 15;
```
Get Post's title,url,score,body score,title score, and date from posts sorted by post body score.
```
SELECT title,url,score,body_score,title_score,DATE(epoch,'unixepoch') FROM posts ORDER BY body_score DESC;
```

Get Post's title,url,score,body score,title score, and date for posts with a title score higher then 1, and not written by u/AutoModerator.
```
SELECT title,url,score,body_score,title_score,DATE(epoch,'unixepoch') FROM posts WHERE title_score>1 AND author!='AutoModerator';
```

Get Post's title,url,score,body score,title score, and date for posts written in last 7 days, sorted by title score.
```
SELECT title,url,score,body_score,title_score,DATE(epoch,'unixepoch') FROM posts WHERE epoch > strftime('%s',CURRENT_TIMESTAMP)-(7*24*60*60) ORDER BY title_score;
```

