#!/usr/bin/python3
import sqlite3
from snooconf import db
with open('snoo.sql') as fp:
    script = fp.read()
sql = sqlite3.connect(db)
sql.executescript(script)
sql.commit()
sql.close()


