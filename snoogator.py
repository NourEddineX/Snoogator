#!/usr/bin/python3
import requests, json, sqlite3, lxml.html, sys
from markdown import Markdown
from snooconf import body_scoring, title_scoring, scoring_threshold, db, ua, rounds, mode

def md2txt(markdowned):
    md = Markdown()
    try:
        htmldoc = md.convert(markdowned)
        doc = lxml.html.fromstring(htmldoc)
        return doc.text_content()
    except lxml.etree.ParserError:
        return markdowned

def post_analyze(c):
    title_score, body_score = 0 , 0 
    bodywords = md2txt(c['selftext']).lower().strip()
    titlewords= c['title'].lower()
    for ts in title_scoring:
        if ts in titlewords:
            title_score = title_score + title_scoring[ts] 
    for bs in body_scoring:
        if bs in bodywords:
            body_score = body_score + body_scoring[bs] 
    score = (body_score * scoring_threshold) + title_score
    return (c['subreddit'],c['url'],c['name'],c['title'],c['selftext'],score,body_score,title_score,c['created_utc'],c['author'])

def fetch_query(q,after):
    if after:
        q.update({'after':after})
    r = requests.get('https://www.reddit.com/search.json',params=q,headers=ua)
    return r

def fetch_url(q,after):
    if after:
        q = q+'&after='+after
    r = requests.get(q,headers=ua)
    return r
##################################
##################################
if __name__ == '__main__':
    if mode == 'query':
        from snooconf import queries as qs
        fetch = fetch_query
    elif mode == 'url':
        from snooconf import query_urls as qs
        fetch = fetch_url
    else:
        print('Bad mode')
        sys.exit(1)
    after = None
    for q in qs:
        for i in range(rounds):
            r = fetch(q,after)
            jcontent = json.loads(r.content)
            sql = sqlite3.connect(db)    
            after = jcontent['data']['after']
            for child in jcontent['data']['children']:
                c = child['data']
                data = post_analyze(c)
                sql.execute('INSERT OR IGNORE INTO posts (subreddit,url,fullname,title,body,score,body_score,title_score,epoch,author,after) VALUES (?,?,?,?,?,?,?,?,?,?,?)',data+(after,))
            sql.commit()
            sql.close()
